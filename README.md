# SPARQL Repository Application v.1.1

![Semmtech Logo](images/semmtech.png)

## Introduction

**SPARQL**, which is an abbreviated as **S**imple **P**rotocol **A**nd **R**DF **Q**uery **L**anguage, is used to define queries for semantic web data (RDF). It is not very different from SQL and even uses some of the same keywords to define queries. However instead of querying a database, SPARQL queries are used to query triples stored in a triplestore.  

For this test case we would like to provide users with a web application for keeping track of useful queries, or find queries already created by others.

## Your Challenge

Your challenge - should you choose to accept it - is to build a web application, which allows its users to find, or create, useful SPARQL queries. And showcase your coding skills, when it comes to developing JavaScript applications. 

### Requirements

The following functional requirements apply to the to be developed web application:

* list existing queries
* create new queries
* modify exiting queries
* delete queries
* execute queries and view results (*optional*)

All users will be using modern web browser like Chrome, Firefox or Edge. Usability of the application from mobile devices, would be nice, but is no priority at the moment.

We believe that we can get a good impression of your JavaScript development skills, based on the requirements above. However, should you want a bigger challenge, we have some additional nice-to-haves: sorting, filtering, syntax highlighting or any magic you want to show add!

### Development Stack

Use the following frameworks and tools: TypeScript, React, NPM, and Webpack.

### Documentation

Don't forget to also provide the clear documentation, as other developer will need to understand how you designed the application. Also include clear instructions on how to build, configure, run from scratch and also on how to access the application.

If you have any questions, please contact [Mike Henrichs](mailto:mikehenrichs@semmtech.nl).

***Good luck!***


## Back-end REST API 

As we often use a service oriented architecture, we have already implemented a simple back-end REST API using Java (using Dropwizard.io). With this API you will be able to perform the basic operations necessary to retrieve and store SPARQL queries. You will need to use this simple API as you back-end, as it will allow you to focus on the actual front-end of the application.

The services are available at the following address: [http://challenge.semmtech.com/sparql-cabinet/api/](http://challenge.semmtech.com/sparql-cabinet/api/) (see API Definition below for details).

### Swagger 

Within Semmtech we use **Swagger** definitions to describe our RESTful APIs. Swagger has a number of useful tools to design, build, document, and consume these APIs. You might want to have a look at [http://swagger.io/](http://swagger.io/) for useful information about working with Swagger definitions, it is even possible to generate a JavaScript client using their code generation tool. If you wish to write a API client from scratch, this is allowed as well.   

The definition for the API to list, create, modify and remove SPARQL queries can be found at [http://challenge.semmtech.com/sparql-cabinet/swagger-ui/swagger.json](http://challenge.semmtech.com/sparql-cabinet/swagger-ui/swagger.json).

This JSON definition can also be viewed within a more user friendly **Swagger UI** by opening [http://challenge.semmtech.com/sparql-cabinet/swagger-ui/](http://challenge.semmtech.com/sparql-cabinet/swagger-ui/) (see [GitHub](https://github.com/swagger-api/swagger-ui) for information about Swagger UI).
 
### API Definition

The following table defines the format of a SPARQL Query (JSON) object. You will need to provide this object when creating or modifying queries, or receive it when retrieving queries through the GET operations.

| Name | Description | Type | Example |
|---|---|---|---|
|**id**<br>*required*          | ID of query (URL-friendly characters)  | string | `"all-statements"` |
|**creator**<br>*required*     | Creator of the query                   | string | `"Mike Henrichs"` |
|**name**<br>*required*        | Short name for query                   | string | `"Select All"` |
|**description**<br>*optional* | Description of the query               | string | `"This query returns the subject, predicate and object of all statements."` |
|**query**<br>*required*       | Creator of query                       | string | `"select ?s ?p ?o { ?s ?p ?o . }"` |

Note that the ID must be unique, and that it will also be used in the URL path to retrieve the query later (see Paths), so only use do not use exotic characters, as this will cause problems in the URLs!

The back-end API provides the following five services: 

| Method | Path | Description |
|---|---|---|
| GET    | `/api/sparql/queries`               | Returns all SPARQL queries in JSON (see Definition). |
| POST   | `/api/sparql/queries`               | Creates a new SPARQL query using provided JSON in body.<br>The header 'Content-Type' of request should therefor be set to `application/json`.<br>The full URI at which the query is available will be returned in the 'Location' header in the response. |
| GET    | `/api/sparql/queries/{id:[^\/#?]+}` | Returns the SPARQL query associated with `id`  |
| PUT    | `/api/sparql/queries/{id:[^\/#?]+}` | Modifies the SPARQL query associated with `id` using provided JSON in body.<br>The header 'Content-Type' of request should therefor be set to `application/json`. | 
| DELETE | `/api/sparql/queries/{id:[^\/#?]+}` | Removes the SPARQL query associated with `id` |

In some of the services above, the path contains a segment`{id:[^\/#?]+}`, which is used to identify a single query to either retrieve, modify or remove. As stated above it must be a string of minimally one character, which does not contain one of the following characters: `/`, `#`, or `?`.

All calls listed in the table should be accompanied by a querystring parameter `?api_key={your-api-key}` which is needed to authenticate your request. This key will be provided to you before starting with this challenge. 

Example: In order to delete the query which has as its ID `all-statements`, you should do a DELETE request on the following URL `http://challenge.semmtech.com/sparql-cabinet/api/sparql/queries/all-statements?api_key=my-key` (`my-key` is the API key provided to you).

## SPARQL Queries (Informative)

In case you are wondering what a SPARQL query looks like, we provide you with a couple of examples as an extra. More information about SPARQL can be found at [SPARQL 1.1 Query Language](https://www.w3.org/TR/sparql11-query/#basicpatterns).

### Query Examples

In order to help you create some SPARQL query to be stored within the repository, we give some basic examples of the query syntax.

#### Statement Count

Returns the number of statements bound to the variable `?size`.

```sparql
select (count(*) as ?size) 
where { 
    ?s ?p ?o . 
}
```

#### All Statements

Returns all statements and the subject, predicate, and object of each statement bound to `?s`, `?p`, `?o` variables respectively.

```sparql
select ?s ?p ?o 
where { 
    ?s ?p ?o . 
} 
order by ?s ?p ?o
```

#### Find Classes

Returns all class names.
 
```sparql
select ?name 
where { 
    ?class a owl:Class . 
    ?class rdfs:label ?name . 
}
order by ?name
```

#### Class Hierarchy

Shows the class hierachy (of named classes) by listing each class and its super class.

```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select ?class ?superClass
where { 
    ?class a owl:Class .
  	?class rdfs:subClassOf ?superClass .
    filter (isIRI(?class) && isIRI(?superClass)) .
} 
order by ?class
```

#### Pizza Descriptions

This query is intended for the pizza model.
 
```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX pizza: <http://www.co-ode.org/ontologies/pizza/pizza.owl#>
select ?name ?description ?pizza
where { 
    ?pizza a owl:Class .
    ?pizza rdfs:subClassOf+ pizza:Pizza .
  	?pizza rdfs:label ?name .
    optional { ?pizza rdfs:comment ?description } .
} 
order by ?name
```

#### Pizza Toppings

Another SPARQL query specifically intended for the pizza model. It returns toppings for each pizza defined in the model.

```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX pizza: <http://www.co-ode.org/ontologies/pizza/pizza.owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select ?name ?topping
where { 
    ?class owl:onProperty pizza:hasTopping .
    ?class owl:someValuesFrom ?topping .
    ?pizza rdfs:subClassOf ?class . 
    ?pizza rdfs:label ?name .
}
order by ?name
```

## SPARQL End-Points (Optional)

As discussed above, executing a stored SPARQL on a **SPARQL End-Point**, and visualizing the results, is optional and added as a bonus. The reason for including it though is to give you a (very small) glimpse into the word of Semantic Web and a way to interact with it using SPARQL.  

### Pizza Model

The following model is taken from: [http://protege.stanford.edu/ontologies/pizza/pizza.owl](http://protege.stanford.edu/ontologies/pizza/pizza.owl). It has been added as an `.owl` file to this project in the folder `models/`.

Some usefull URIs for this model, which you might want to use in your queries, are:

* `pizza:Food`
* `pizza:Pizza`
* `pizza:PizzaTopping`
* `pizza:hasIngredient`

Note that the prefix `pizza:` is a shorthand for the namespace `http://www.co-ode.org/ontologies/pizza/pizza.owl#`. And as a consequence `pizza:Food` is an abbreviation of the rather bulky URI `http://www.co-ode.org/ontologies/pizza/pizza.owl#Food`.  

### Executing Queries

We have configured a SPARQL end-point for the pizza model on a **RDF4J Server**, which located at `http://sparql-vps02.semmtech.com/`. In order to contact it, you may use the URL [http://sparql-vps02.semmtech.com/rdf4j-server/repositories/pizza](http://sparql-vps02.semmtech.com/rdf4j-server/repositories/pizza).

In order to execute a SPARQL query on the configured end-points is using a POST request. Only two headers should be provided, the first is the 'Content-Type', which will be `application/sparql-query`. The other header will be the acceptable format of the result, which can be set using the 'Accept' header and one of the following values:
 
 * `text/csv` for comma separated format
 * `text/tab-separated-values` for tabs separated format
 * `application/sparql-results+xml` for XML format
 * `application/sparql-results+json` for JSON format
 
For more information about the SPARQL 1.1 HTTP Protocol, see [documentat at W3C](https://www.w3.org/TR/sparql11-protocol/#protocol).

If your are interested in the RDF4J, see the [Eclipse RDF4J Documentation](http://rdf4j.org/). 
