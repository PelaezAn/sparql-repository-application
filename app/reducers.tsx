import { initialState } from "./initialState";
import * as actionType from "./actionTypes";
import { Action, State } from "./types";

export function rootReducer(state: State = initialState, action: Action)
 {
    switch (action.type) {
        case actionType.RECEIVE_QUERIES: {
            return {
                ...state,
                queries: action.payload
            }
        }
        case actionType.SELECT_QUERY: {
            return {
                ...state,
                selectedQuery: action.payload
            };
        }
        case actionType.RECEIVE_QUERY_EXECUTION: {
            return {
                ...state,
                lastQueryResult: action.payload
            };
        }
        case actionType.SAVE_QUERY: {
            return {
                ...state,
                queries: { [action.payload.id]: action.payload, ...state.queries },
                selectedQuery: action.payload.id
            };
        }
        case actionType.DELETE_QUERY: {
            let queries = { ...state.queries };
            delete queries[action.payload];
            return {
                ...state,
                selectedQuery: state.selectedQuery === action.payload ? '': state.selectedQuery,
                queries: { ...queries }
            };
        }
        case actionType.MARK_WITH_ERROR: {
            let query = { ...state.queries[action.payload.id] };
            query.error = action.payload.error;
            return {
                ...state,
                queries: { ...state.queries, [query.id]: query }
            };
        }
        case actionType.GENERAL_ERROR: {
            return {
                ...state,
                error: action.payload
            };
        }
        case actionType.CLEAR_SELECTION: {
            return {
                ...state,
                selectedQuery: ''
            };
        }
        case actionType.SELECT_QUERY: {
            return {
                ...state,
                selectedQuery: action.payload
            };
        }
        default:
            return state;
    }
}