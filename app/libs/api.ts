import { Query } from "../types";
const BASE_URL = "http://challenge.semmtech.com/sparql-cabinet/api";
const API_KEY = (<any>window).API_KEY;

/**
 * Fetches all queries
 * @returns {Promise}
 */
export function getQueries(): Promise<any> {
    return fetch(`${BASE_URL}/sparql/queries?api_key=${API_KEY}`, {
        method: 'GET'
    });
}

/**
 * Saves a new query
 * @param {Query} query
 * @returns {Promise}
 */
export function createQuery(query: Query): Promise<any> {
    return fetch(`${BASE_URL}/sparql/queries?api_key=${API_KEY}`, {
        method: 'POST',
        body: JSON.stringify(query),
        headers: {
          'Content-Type': 'application/json'
        }
    });
}

/**
 * Updates an existing query
 * @param {Query} query
 * @returns {Promise}
 */
export function updateQuery(query: Query): Promise<any> {
    return fetch(`${BASE_URL}/sparql/queries/${query.id}?api_key=${API_KEY}`, {
        method: 'PUT',
        body: JSON.stringify(query),
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Headers': 'Content-Type'
        }
    });
}

/**
 * Deletes an existing query
 * @returns {Promise}
 */
export function deleteQuery(id: string): Promise<any> {
    return fetch(`${BASE_URL}/sparql/queries/${id}?api_key=${API_KEY}`, {
        method: 'DELETE'
    });
}
