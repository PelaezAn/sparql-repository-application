const BASE_URL = 'http://sparql-vps02.semmtech.com/rdf4j-server/repositories/pizza';

/**
 * Executes the given query
 * @param {String} query
 * @returns {Promise}
 */
export function executeQuery(query: string): Promise<any> {
    return fetch(`${BASE_URL}?query=${encodeURIComponent(query)}`, {
        method: 'GET',
        mode: 'no-cors',
        headers: {
            'Content-Type': 'application/sparql-query',
            'Accept': 'text/tab-separated-values'
        }
    });
}
