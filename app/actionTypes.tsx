export const REQUEST_QUERIES = 'REQUEST_QUERIES';
export const RECEIVE_QUERIES = 'RECEIVE_QUERIES';
export const SELECT_QUERY = 'SELECT_QUERY';
export const EXECUTE_QUERY = 'EXECUTE_QUERY';
export const RECEIVE_QUERY_EXECUTION = 'RECEIVE_QUERY_EXECUTION';
export const SAVE_QUERY = 'SAVE_QUERY';
export const UPDATE_QUERY = 'UPDATE_QUERY';
export const DELETE_QUERY = 'DELETE_QUERY';
export const MARK_WITH_ERROR = 'MARK_WITH_ERROR';
export const GENERAL_ERROR = 'GENERAL_ERROR';
export const CLEAR_SELECTION = 'CLEAR_SELECTION';
