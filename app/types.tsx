export type Action = {
    payload?: any,
    type: string
};

export type Query = {
    id: string,
    creator: string,
    name: string,
    description?: string,
    query: string,
    error?: string
};

export interface Queries {
    [index: string]: Query
};

export type State = {
    readonly queries: Queries,
    readonly selectedQuery: string,
    readonly lastQueryResult: string
    readonly error?: string
};