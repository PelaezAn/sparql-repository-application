import * as React from 'react';
import { connect } from 'react-redux';
import { SemInput } from './SemInput';
import { Query, Queries } from "../types";
import { insertQuery, clearSelection, updateQuery, executeQuery } from "../actions";

export interface ComponentProps {
    selectedQuery: string,
    queries: Queries,
    saveQuery: Function,
    updateQuery: Function,
    clearSelection: Function,
    execute: Function
}

interface State {
    id: string,
    creator: string,
    name: string,
    description: string,
    query: string,
    queryError: string
}

class QueryConsole extends React.Component<ComponentProps, State> {
    private idInput: React.RefObject<SemInput>;
    private authorInput: React.RefObject<SemInput>;
    private nameInput: React.RefObject<SemInput>;
    private descriptionInput: React.RefObject<SemInput>;

    constructor(props) {
        super(props);
        this.state = { id: '', creator : '', name: '', description: '', query: '', queryError: '' }
        this.idInput = React.createRef();
        this.authorInput = React.createRef();
        this.nameInput = React.createRef();
        this.descriptionInput = React.createRef();
    }

    /**
     * Clears the query information
     */
    clear = () => {
        this.setState({ id: '', creator : '', name: '', description: '', query: '' });
        this.props.clearSelection();
    }

    /**
     * Saves or updates the given query
     */
    save = () => {
        const { saveQuery, selectedQuery, updateQuery } = this.props;
        const { queryError, ...queryData } = this.state;
        if (!queryData.query) {
            return this.setState({ queryError: "Empty query" });
        }

        if (!queryData.id) {
            return this.idInput.current.setError("Value is required");
        } else if (/(#|\s|\?)/g.test(queryData.id)) {
            return this.idInput.current.setError("Invalid ID. It cannot contain whitespaces or / #");
        }

        if (!queryData.creator) {
            return this.authorInput.current.setError("Value is required");
        }

        if (!queryData.name) {
            return this.nameInput.current.setError("Value is required");
        }

        if (!!selectedQuery) {
            updateQuery(queryData);
        } else {
            saveQuery(queryData);
        }
    }

    /**
     * On change query
     */
    onChangeQuery = (e: any) => {
        this.setState({ query: e.target.value, queryError: '' });
    }

    componentWillReceiveProps(nextProps: ComponentProps) {
        const { selectedQuery, queries } = nextProps;
        if (!!nextProps.selectedQuery && selectedQuery !== this.props.selectedQuery) {
            let query = queries[selectedQuery];
            if (!!query) {
                this.setState({
                    id: query.id,
                    creator: query.creator,
                    name: query.name,
                    description: query.description,
                    query: query.query,
                    queryError: ''
               }); 
            } else {
                this.clear();
            }
        }
    }

    render() {
        return (
            <div id="query-console">

                <SemInput
                    label={"Query ID"} 
                    name={"id"}
                    required
                    disabled={!!this.props.selectedQuery}
                    value={this.state.id}
                    onChange={(text) => this.setState({ id: text })}
                    ref={this.idInput}
                />

                <SemInput 
                    label={"Name"} 
                    name={"Query name"}
                    required
                    value={this.state.name}
                    onChange={(text) => this.setState({ name: text })}
                    ref={this.nameInput}
                />

                <SemInput 
                    label={"Author"} 
                    name={"author"}
                    required
                    value={this.state.creator}
                    onChange={(text) => this.setState({ creator: text })}
                    ref={this.authorInput}
                />

                <SemInput 
                    label={"Description"} 
                    name={"description"}
                    required={false}
                    value={this.state.description}
                    onChange={(text) => this.setState({ description: text })}
                    ref={this.descriptionInput}
                />

                <div className="console-container">
                    <textarea id="console" onChange={this.onChangeQuery} value={this.state.query} />
                    <p className="query-error">{ this.state.queryError }</p>
                    <div className="actions">
                        <input type="button" value={!!this.props.selectedQuery ? "update" : "save"} onClick={this.save}/>
                        <input type="button" value={"execute"} onClick={() => this.props.execute(this.state.query)}/>
                        <input type="button" value={"clear"} onClick={this.clear}/>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedQuery: state.selectedQuery,
        queries: state.queries
    };
};

function mapDispatchToProps(dispatch) {
    return {
        saveQuery: (query: Query) => dispatch(insertQuery(query)),
        updateQuery: (query: Query) => dispatch(updateQuery(query)),
        clearSelection: () => dispatch(clearSelection()),
        execute: (query: string) => dispatch(executeQuery(query))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(QueryConsole);
