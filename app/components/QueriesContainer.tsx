import * as React from 'react';
import { connect } from 'react-redux';
import { Query, Queries } from '../types';
import { fetchQueries, selectQuery, removeQuery } from '../actions';

export interface ComponentProps {
    queries: Queries,
    selectedQuery: string,
    requestQueries: Function,
    selectQuery: Function,
    removeQuery: Function 
}

interface State {
    page: number
}

export interface QueryElementProps {
    query: Query,
    isSelected: boolean,
    onClick: Function,
    onClickOnDelete: Function
}

const QueryElement: React.SFC<QueryElementProps> = (props) => {
    const { query, isSelected, onClick,  onClickOnDelete } = props;

    return (
        <li className={`query-container ${isSelected ? 'active': ''}`}  onClick={() => onClick()}>
            <p><b>ID: </b>{ query.id }<span className="query-container-delete" onClick={() => onClickOnDelete()}>X</span></p>
            <p><b>Name: </b>{ query.name }</p>
            <p><b>Creator: </b>{ query.creator }</p>
        </li>
    );
};

class QueriesContainer extends React.Component<ComponentProps, State> {

    constructor(props: ComponentProps) {
        super(props);
        this.state = { page: 1 };
    }

    componentDidMount() {
        this.props.requestQueries();
    }

    render() {
        const { queries, selectedQuery, selectQuery, removeQuery } = this.props;
        const { page } = this.state;
        const queryElements = [];
        for(const key in queries) {
            if (queries.hasOwnProperty(key)) {
                let query: Query = queries[key];
                queryElements.push(
                    <QueryElement
                        key={`query-container-${query.id}`} 
                        query={query}
                        isSelected={query.id === selectedQuery }
                        onClick={() => selectQuery(query.id)}
                        onClickOnDelete={() => removeQuery(query.id)}
                    />
                );
            }
        }
        return(
            <div id="queries-container">
                <ul>
                    { queryElements }
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        queries: state.queries,
        selectedQuery: state.selectedQuery
    };
};

function mapDispatchToProps(dispatch) {
    return {
        requestQueries: () => dispatch(fetchQueries()),
        selectQuery: (id: string) => dispatch(selectQuery(id)),
        removeQuery: (id: string) => dispatch(removeQuery(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(QueriesContainer);
