import * as React from 'react';

export interface State {
    error: string
}

export interface Props {
    name: string,
    label: string,
    onChange: Function,
    value: string,
    required: boolean,
    disabled?: boolean
}

export class SemInput extends React.PureComponent<Props, State> {

    constructor(props) {
        super(props);
        this.state = { error: '' };
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (!this.props.disabled) {
            const { value } = e.target;
            this.props.onChange(value);
            this.setError('');
        }
    }

    /**
     * Sets an error in the input
     */
    public setError = (error: string) => this.setState({ error })

    render() {
        const { value, name, required, label, disabled } = this.props;
        const { error } = this.state;

        return (
            <div className="row sem-input">
                <label htmlFor="id">{ label }</label>
                <input
                    name={ name}
                    type="text"
                    value={value}
                    onChange={this.handleChange}
                    disabled={disabled}
                    placeholder={ required ? 'Required' : 'Optional' }
                />
                <p>{ error }</p>
            </div>
        );
    }
}
