import * as React from 'react';
import { connect } from 'react-redux';

export interface ComponentProps {
    readonly lastQueryResult: string
}

interface State {
}

class Results extends React.Component<ComponentProps, State> {

    render() {
        const { lastQueryResult } = this.props;
        return(
            <div className="row">
                <div id="query-results">
                    <span>{ lastQueryResult }</span>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        lastQueryResult: state.lastQueryResult
    };
};


export default connect(mapStateToProps)(Results);
