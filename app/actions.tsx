import { ThunkDispatch } from 'redux-thunk';
import { Action, State, Queries, Query } from "./types";
import * as actionTypes from "./actionTypes";
import * as api from "./libs/api";
import * as sparql from "./libs/sparql";

/**
 * Sets the queries received from the server
 * @param queries 
 */
function receiveQueries(queries: Queries): Action {
    return {type: actionTypes.RECEIVE_QUERIES, payload: queries };
}

/**
 * Sets a general error
 * @param error
 */
function generalError(error: string): Action {
    return {type: actionTypes.GENERAL_ERROR, payload: error };
}

/**
 * 
 * @param id 
 * @param error 
 */
function markQueryWithError(id:string, error: string): Action {
    return {type: actionTypes.MARK_WITH_ERROR, payload: {id, error} };
}

/**
 * 
 * @param query 
 */
function saveQuery(query: Query): Action {
    return {type: actionTypes.SAVE_QUERY, payload: query };
}

/**
 * 
 * @param query 
 */
function deleteQuery(id: string): Action {
    return {type: actionTypes.DELETE_QUERY, payload: id };
}

/**
 * 
 * @param query 
 */
export function selectQuery(id: string): Action {
    return {type: actionTypes.SELECT_QUERY, payload: id };
}


/**
 * 
 * @param result 
 */
function receiveQueryExecution(result: any): Action {
    return {type: actionTypes.DELETE_QUERY, payload: result };
}

/**
 * 
 */
export function clearSelection() : Action {
    return { type: actionTypes.CLEAR_SELECTION };
}

/**
 * Fetches all queries
 */
export function fetchQueries() {
    return async (dispatch: ThunkDispatch<State, void, Action>) => {
        try {
            const response = await api.getQueries();
            if (response.ok) {
                const queries = await response.json();
                const formatted: Queries = {};
                for (let i = 0, n = queries.length; i < n; i++) {
                    let query = queries[i];
                    formatted[query.id] = query;
                }
                return dispatch(receiveQueries(formatted));
            } else {
                return dispatch(generalError("Error retrieving queries. Please try again in a few minutes."));
            }
        } catch (error) {
            console.error(error);
        } 
    };
}

/**
 * Saves a new query
 */
export function insertQuery(query: Query) {
    return async (dispatch: ThunkDispatch<State, void, Action>) => {
        try {
            dispatch(saveQuery(query));
            const { error, ...queryData } = query;
            const response = await api.createQuery(queryData);
            if (response.ok) {
                return null;
            } else if (response.status === 404){
                dispatch(generalError("ID is already in use"));
                return markQueryWithError(query.id, "ID is already in use");
            } else {
                dispatch(generalError("Error saving query"));
                return markQueryWithError(query.id, "Error saving query. Please try again in a few minutes.");
            }
        } catch (error) {
            console.error(error);
        }
    };
}

/**
 * Updates an existing query
 * @param {Query} query
 */
export function updateQuery(query: Query) {
    return async (dispatch: ThunkDispatch<State, void, Action>) => {
        try {
            dispatch(saveQuery(query));
            //remove unnecesary fields
            const { error, ...queryData } = query;
            const response = await api.updateQuery(queryData);
            if (response.ok) {
                return null;
            } else if (response.status === 404){
                dispatch(generalError("ID cannot be found"));
                return markQueryWithError(query.id, "ID cannot be found");
            } else {
                dispatch(generalError("Internal error updating query"));
                return markQueryWithError(query.id, "Internal error updating query. Please try again in a few minutes.");
            }
        } catch (error) {
            console.error(error);
        }
    };
}

/**
 * Updates an existing query
 * @param {Query} query
 */
export function removeQuery(id: string) {
    return async (dispatch: ThunkDispatch<State, void, Action>) => {
        try {
            dispatch(deleteQuery(id));
            const response = await api.deleteQuery(id);
            if (response.ok) {
                return null;
            } else if (response.status === 404){
                dispatch(generalError("ID cannot be found"));
                return markQueryWithError(id, "ID cannot be found");
            } else {
                dispatch(generalError("Internal error deleting query"));
                return markQueryWithError(id, "Internal error deleting query. Please try again in a few minutes.");
            }
        } catch (error) {
            console.error(error);
        }
    };
}

/**
 * Updates a query
 * @param {Query} query
 */
export function executeQuery(query: string) {
    return async (dispatch: ThunkDispatch<State, void, Action>) => {
        try {
            const response = await sparql.executeQuery(query);
            console.dir(response);
            const text = await response.text();
            console.log(text);
            if (true) {
                return dispatch(receiveQueryExecution(text));
            } 
        } catch (error) {
            console.error(error);
            dispatch(generalError("Error executing query"));
        }
    };
}