# Semmtech technical assessment

## Installing

To get the project running you will need to follow the following steps. Install all required modules using the node package manager in the root folder.

```
npm i
```

You will also need to bundle all client-side javascript files. You can do so with the following command in the same directory.

```
webpack
```

Now you can locally access through your browser in the following direction
```
    $PATH_TO_YOUR_WORKSPACE/sparql-repository-application/index.html
```
