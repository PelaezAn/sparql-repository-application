import { State } from "./types";

export const initialState = {
    queries: {},
    selectedQuery: '',
    lastQueryResult: '',
    error: ''
};
