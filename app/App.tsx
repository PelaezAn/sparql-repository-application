import { Action } from 'redux';
import { connect } from 'react-redux';
import * as React from 'react';
import { ThunkDispatch } from 'redux-thunk';
import { State } from "./types";
import Results from './components/Results';
import QueriesContainer from './components/QueriesContainer';
import QueryConsole from './components/QueryConsole';

interface AppProps {
    dispatch: ThunkDispatch<State, void, Action>
};

class App extends React.Component<AppProps> {
    render() {
      const {  dispatch } = this.props;
  
      return (
        <div className="row">
          <div className="row">
            <QueriesContainer />
            <QueryConsole />
          </div>
          <Results />
        </div>
      );
    }
  }
  
  export default connect()(App);
